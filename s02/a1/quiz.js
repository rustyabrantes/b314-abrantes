// 1. What is the term given to unorganized code that's very hard to work with?
// ans. Spaghetti code

// 2. How are object literals written in JS?
// ans. inside {} to create an object representing a user

// 3. What do you call the concept of organizing information and functionality to belong to an object?
// ans. Encapsulation

// 4. If studentOne has a method named enroll(), how would you invoke it?
// ans. studentOne.enroll()

// 5. True or False: Objects can have objects as properties.
// ans. True

// 6. What is the syntax in creating key-value pairs?
// ans. key: value

// 7. True or False: A method can have no parameters and still work.
// ans. True

// 8. True or False: Arrays can have objects as elements.
// ans. True

// 9. True or False: Arrays are objects.
// ans. True

// 10. True or False: Objects can have arrays as properties.
// ans. True