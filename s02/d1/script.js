// Use an object literal: {} to create an object representing a user
	// Encapsulation
	// Whenever we add properties or methods to an object, we are performing ENCAPSULATION
	// Organization of information (as properties) and behavior (as methods) to belong to the object that encapsulates them
	// (the scope of encapsulation is denoted by object literals)

let studentOne = {
	name: 'John',
	email: 'john@mail.com',
	grades: [89, 84, 78, 88],

	// Methods
		// Add the functionalities available to a student as object  methods
		// The keyword "this" refers to the object encapsulating the method where "this" is called

	login(){
		console.log(`${this.email} has logged in`)
	},
	logout(){
		console.log(`${this.email} has logged out`)
	},
	listGrades(){
		console.log(`${this.name}'s quarterly grade averages are ${this.grades}`)
	},

	computeAve(){
        let sum = 0;
        this.grades.forEach(grade => sum = sum + grade);
        return sum/4;
	},
	willPass() {
		if(this.computeAve() >= 85){
			return true;
		} else {
			return false;
		}
	},
	willPassWithHonors() {
	    if(this.computeAve() >= 90){
	    	return true;
	    }
	    if(this.computeAve() < 85){
	    	return undefined;
	    }
	    if(this.computeAve() < 90 && this.computeAve() >= 85){
	    	return false;
	    }
	}
}


// log the contents of studentOne's encapsulated information in the console
console.log(`Student One's name is ${studentOne.name}`);
console.log(`Student One's email is ${studentOne.email}`);
console.log(`Student One's quarterly grade averages are ${studentOne.grades}`);
console.log(studentOne);


// A C T I V I T Y

// 1. Translate the other students from our boilerplate code into their own respective objects.
// 2. Define a method for EACH student object that will compute for their grade average (total of grades divided by 4)
// 3. Define a method for all student objects named willPass() that returns a Boolean value indicating if student will pass or fail.
//For a student to pass, their ave. grade must be greater than or equal to 85.
// 4. Define a method for all student objects named willPassWithHonors() that returns true if ave. grade is greater than or equal to 90,
//false if >= 85 but < 90, and undefined if < 85 (since student will not pass).
let studentTwo = {
	name: 'Joe',
	email: 'joe@mail.com',
	grades: [78, 82, 79, 85],

	computeAve(){
        let sum = 0;
        this.grades.forEach(grade => sum = sum + grade);
        return sum/4;
	},
	willPass() {
		if(this.computeAve() >= 85){
			return true;
		} else {
			return false;
		}
	},
	willPassWithHonors() {
	    if(this.computeAve() >= 90){
	    	return true;
	    }
	    if(this.computeAve() < 85){
	    	return undefined;
	    }
	    if(this.computeAve() < 90 && this.computeAve() >= 85){
	    	return false;
	    }
	}
}

let studentThree = {
	name: 'Jane',
	email: 'jane@mail.com',
	grades: [87, 89, 91, 93],

	computeAve(){
        let sum = 0;
        this.grades.forEach(grade => sum = sum + grade);
        return sum/4;
	},
	willPass() {
		if(this.computeAve() >= 85){
			return true;
		} else {
			return false;
		}
	},
	willPassWithHonors() {
	    if(this.computeAve() >= 90){
	    	return true;
	    }
	    if(this.computeAve() < 85){
	    	return undefined;
	    }
	    if(this.computeAve() < 90 && this.computeAve() >= 85){
	    	return false;
	    }
	}
}

let studentFour = {
	name: 'Jessie',
	email: 'jessie@mail.com',
	grades: [91, 89, 92, 93],

	computeAve(){
        let sum = 0;
        this.grades.forEach(grade => sum = sum + grade);
        return sum/4;
	},
	willPass() {
		if(this.computeAve() >= 85){
			return true;
		} else {
			return false;
		}
	},
	willPassWithHonors() {
	    if(this.computeAve() >= 90){
	    	return true;
	    }
	    if(this.computeAve() < 85){
	    	return undefined;
	    }
	    if(this.computeAve() < 90 && this.computeAve() >= 85){
	    	return false;
	    }
	}
}

// 5. Create an object named classOf1A with a property named students which is an array containing all 4 student objects in it.
// 6. Create a method for the object classOf1A named countHonorStudents() that will return the number of honor students.
// 7. Create a method for the object classOf1A named honorsPercentage() that will return the % of honor students from the batch's total number of students.
// 8. Create a method for the object classOf1A named retrieveHonorStudentInfo() that will return all honor students' emails and ave. grades as an array of objects.
//9. Create a method for the object classOf1A named sortHonorStudentsByGradeDesc() that will return all honor students' emails and ave. grades as an array of objects sorted in descending order based on their grade averages.

let classOf1A = {
	students: [studentOne, studentTwo, studentThree, studentFour],
	countHonorStudents(){
		let result = 0;
		this.students.forEach(student => {
			if(student.willPassWithHonors() === true){
				result ++
			}
			}
		)
		return result;
	},
	honorsPercentage(){
		let percent = (this.countHonorStudents()/this.students.length)*100
		return percent;
	},
	retrieveHonorStudentInfo(){
		let honorStudents = [];
		this.students.forEach(student => {
			if(student.willPassWithHonors())
				honorStudents.push({
					email: student.email,
					aveGrade: student.computeAve()
				})
		})
		return honorStudents;
	},
	sortHonorStudentsByGradeDesc(){
		return this.retrieveHonorStudentInfo().sort((studentA, studentB) => {
			return studentB.aveGrade - studentA.aveGrade;
		})

	}

}

